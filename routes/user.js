/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var express = require('express');
var router = express.Router();
var sendResponse = require('./sendResponse');
var func = require('./commonFunction');

router.post('/send_email',function(req, res) {
    
    var email = req.body.email;
    var sub = req.body.sub;
    var message = req.body.message;
    
    var data=[email,sub,message];
    
    var checkdata=func.checkBlank(data)
    
    if (checkdata==1) {
         sendResponse.parameterMissingError(res);
    }
    else
    {
        func.sendEmail(email, message, sub, function (result) {
        if (result === 1) {
            sendResponse.sendSuccessData(data, res);

        }
        else {
            sendResponse.sendErrorMessage("error", res);
        }
    });
        
    }
    
    
});

module.exports = router;
