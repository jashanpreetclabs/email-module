var config=require('config');
var sendResponse = require('./sendResponse');
var nodemailer = require("nodemailer");

exports.checkBlank = function(arr) {

    return (checkBlank(arr));
};
function checkBlank(arr) {
    var arrlength = arr.length;
    for (var i = 0; i < arrlength; i++) {
        if (arr[i] == '' || arr[i] == undefined ||arr[i] == '(null)') {
            return 1;
            break;
        }
    }
    return 0;
}

exports.sendEmail = function(receiverMailId, message, subject, callback) {
    
    var smtpTransport = nodemailer.createTransport("SMTP", {
        service: "Gmail",
        auth: {
            user: config.get('emailSettings').email,
            pass: config.get('emailSettings').password
        }
    });

// setup e-mail data with unicode symbols
    var mailOptions = {
        from: config.get('emailSettings').email, 
        to: receiverMailId, 
        subject: subject, 
        text: message, 
        //html: "<b>Hello world ?</b>" // html body
    }

// send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function(error, response) {
        if (error) {
            return callback(0);
        } else {
            //  console.log('hi');
            return callback(1);
        }

        // if you don't want to use this transport object anymore, uncomment following line
        //smtpTransport.close(); // shut down the connection pool, no more messages
    });
};
