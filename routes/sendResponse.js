/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


exports.sendSuccessData = function (data,res) {

    var successResponse = {
        status: '1',
        message: "Successful",
        data: data
    };
    sendData(successResponse,res);
};

exports.sendErrorMessage = function (msg,res) {

    var errResponse = {
        status: '0',
        error: msg,
        data: {}
    };
    sendData(errResponse,res);
};

exports.parameterMissingError = function (res) {

    var errorMsg = {
        error: 'Some parameter missing',
        status:0
    };
    sendData(errorMsg,res);
};
function sendData(data,res)
{
    res.type('json');
    res.jsonp(data);
}


exports.sendData = function (data,res) {

    res.type('json');
    res.jsonp(data);
};